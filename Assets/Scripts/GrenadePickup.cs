﻿using UnityEngine;



public class GrenadePickup : MonoBehaviour
{
    public GrenadeType Type;
}



public enum GrenadeType
{
    Simple,
    Frag,
    Poison,
    Freezing
}
