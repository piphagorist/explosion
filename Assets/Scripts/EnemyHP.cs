﻿using UnityEngine;
using UnityEngine.UI;



public class EnemyHP : MonoBehaviour
{
    [SerializeField] private Image _hpImg;
    [SerializeField] private Text _hpText;

    private Transform _transform;



    public void SetHP( float value )
    {
        _hpImg.fillAmount = value / 100;
        _hpText.text = value.ToString( "f2" ) + "%";
    }


    private void Start()
    {
        _transform = transform;
    }

    private void Update()
    {
        _transform.LookAt( Camera.main.transform );
    }
}
