﻿using UnityEngine;



public class Generator : MonoBehaviour
{
    private void Start()
    {
        Generate( Game.GrenadesPickupObj, Game.Params.NumberOfGrenates );
        Generate( Game.EnemiesObj, Game.Params.NubmerOfEnemies );
    }

    private void Generate( GameObject[] obj, int numberOfObj )
    {
        for ( int i = 0; i < numberOfObj; i++ )
        {
            GameObject newObj = Instantiate( obj[ Random.Range( 0, obj.Length ) ] );
            float yRot = Random.Range( 0.0f, 360.0f );
            newObj.transform.position = RandomPointInCircle( transform.position, Game.Params.SpawnRadius );
            newObj.transform.eulerAngles = new Vector3( 0, yRot, 0 );
        }
    }

    private Vector3 RandomPointInCircle( Vector3 center, float radius )
    {
        Vector3 pos = ( Vector2 )center + Random.insideUnitCircle * radius;
        pos.z = pos.y;
        pos.y = 0;

        return pos;
    }
}
