﻿using UnityEngine;



public class FreezingGrenade : Grenade
{
    [SerializeField] private float _duration;



    protected override void OnHit( Collider[] colliders )
    {
        foreach ( Collider col in colliders )
            col.GetComponentInChildren<Enemy>()?.Freeze( _damage, _duration );
    }
}
