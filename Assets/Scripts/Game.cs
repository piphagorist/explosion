﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] private GameParams _params;

    [SerializeField] private Grenade[] _grenades;
    [SerializeField] private GrenadeUI _grenadeUIObj;
    [SerializeField] private RectTransform _grenadesUIContainer;
    [SerializeField] private GameObject[] _enemiesObj;
    [SerializeField] private GameObject[] _grenadesPickupObj;


    public static GameParams Params;
    public static Grenade[] Grenades;
    public static GameObject[] GrenadesObj;
    public static GrenadeUI GrenadeUIObj;
    public static RectTransform GranadesUIContainer;
    public static GameObject[] EnemiesObj;
    public static GameObject[] GrenadesPickupObj;



    private void Awake()
    {
        Params = _params;
        Grenades = _grenades;
        GrenadeUIObj = _grenadeUIObj;
        GranadesUIContainer = _grenadesUIContainer;
        EnemiesObj = _enemiesObj;
        GrenadesPickupObj = _grenadesPickupObj;


        GrenadesObj = new GameObject[ Grenades.Length ];
        for ( int i = 0; i < Grenades.Length; i++ )
            GrenadesObj[ i ] = Grenades[ i ].gameObject;
    }
}
