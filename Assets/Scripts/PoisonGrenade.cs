﻿using UnityEngine;



public class PoisonGrenade : Grenade
{
    [SerializeField] private float _duration;



    protected override void OnHit( Collider[] colliders )
    {
        foreach ( Collider col in colliders )
            col.GetComponentInChildren<Enemy>()?.PoisonHit( _damage, _duration );
    }
}
