﻿using System.Collections;
using UnityEngine;



public class Enemy : MonoBehaviour
{
    [SerializeField] private float _startHP;

    [SerializeField] private EnemyHP _enemyHP;


    private Color _startColor;

    private float _currentHP;

    private float _multiplier = 1.0f;

    private Coroutine _poison;
    private Coroutine _freeze;




    private void Awake()
    {
        _currentHP = _startHP;
        _startColor = GetComponentInChildren<MeshRenderer>().material.color;
    }

    private void GetDamage( float damage )
    {
        _currentHP -= damage * _multiplier;
        _enemyHP.SetHP( _currentHP );

        if ( _currentHP <= 0 )
        {
            _currentHP = 0;
            Destroy( gameObject );
        }
    }


    public void Hit( float damage )
    {
        GetDamage( damage );
    }

    public void PoisonHit( float damage, float duration )
    {
        if ( _poison != null ) StopCoroutine( _poison );
        _poison = StartCoroutine( _PoisonHit( damage, duration ) );
    }

    public void Freeze( float damage, float duration )
    {
        GetDamage( damage );

        if(_freeze != null) StopCoroutine( _freeze );
        _freeze = StartCoroutine( _Freeze( duration ) );
    }


    IEnumerator _PoisonHit( float damage, float duration )
    {
        for ( int i = 0; i < duration; i++ )
        {
            GetDamage( damage );
            yield return new WaitForSeconds( 1.0f );
        }
    }

    IEnumerator _Freeze( float duration )
    {
        GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
        _multiplier = 2.0f;

        yield return new WaitForSeconds( duration );

        GetComponentInChildren<MeshRenderer>().material.color = _startColor;
        _multiplier = 1.0f;
    }
}
