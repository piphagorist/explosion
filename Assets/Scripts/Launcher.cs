﻿using System;
using System.Collections.Generic;
using UnityEngine;



public class Launcher : MonoBehaviour
{
    private GameObject[] _grenadesModels;

    private GrenadeUI[] _grenadesUI;

    private int[] _numberOfGrenades;

    private LineRenderer _lineRenderer;

    private GameObject _selectedGrenade;

    private Transform _transform;

    private bool _aiming;

    private int _grenadeIndex;

    private Dictionary<GrenadeType, int> _grenadeIndexByType = new Dictionary<GrenadeType, int>();



    private void Start()
    {
        _transform = transform;

        _lineRenderer = gameObject.GetComponentInChildren<LineRenderer>();
        _lineRenderer.positionCount = 0;

        _grenadesUI = new GrenadeUI[ Game.Grenades.Length ];
        _grenadesModels = new GameObject[ Game.Grenades.Length ];
        _numberOfGrenades = new int[ Game.Grenades.Length ];

        for ( int i = 0; i < Game.Grenades.Length; i++ )
        {
            _grenadesUI[ i ] = Instantiate( Game.GrenadeUIObj.gameObject, Game.GranadesUIContainer ).GetComponent<GrenadeUI>();
            _grenadesUI[ i ].SetColor( Game.Grenades[ i ].Color );
            _grenadesUI[ i ].SetSelet( false );
            _grenadesUI[ i ].SetNumber( 0 );

            _grenadesModels[ i ] = Instantiate( Game.Grenades[ i ].gameObject, _transform );
            _grenadesModels[ i ].SetActive( false );
            _grenadesModels[ i ].transform.localPosition = Vector3.zero;

            _grenadeIndexByType.Add( Game.Grenades[ i ].Type, i );
        }

        _selectedGrenade = _grenadesModels[ 0 ];
        _grenadesUI[ 0 ].SetSelet( true );
    }

    private void Update()
    {
        if ( Input.GetKeyDown( KeyCode.DownArrow ) )
        {
            for ( int i = 0; i < Game.Grenades.Length; i++ )
                _grenadesUI[ i ].SetSelet( false );

            _grenadeIndex++;
            _grenadeIndex = _grenadeIndex < Game.Grenades.Length ? _grenadeIndex : 0;

            _grenadesUI[ _grenadeIndex ].SetSelet( true );

            _selectedGrenade.SetActive( false );
            _selectedGrenade = _grenadesModels[ _grenadeIndex ];

            if ( _numberOfGrenades[ _grenadeIndex ] > 0 )
                _selectedGrenade.SetActive( true );
        }
        else if ( Input.GetKeyDown( KeyCode.UpArrow ) )
        {
            for ( int i = 0; i < _grenadesUI.Length; i++ )
                _grenadesUI[ i ].SetSelet( false );

            _grenadeIndex--;
            _grenadeIndex = _grenadeIndex < 0 ? Game.Grenades.Length - 1 : _grenadeIndex;

            _grenadesUI[ _grenadeIndex ].SetSelet( true );

            _selectedGrenade.SetActive( false );
            _selectedGrenade = _grenadesModels[ _grenadeIndex ];

            if ( _numberOfGrenades[ _grenadeIndex ] > 0 )
                _selectedGrenade.SetActive( true );
        }


    }

    private void LateUpdate()
    {
        if ( _aiming )
        {
            _lineRenderer.SetPositions( CalculatePositions() );
            _lineRenderer.positionCount = Game.Params.LineSegments;
        }
    }

    private Vector3[] CalculatePositions()
    {
        Vector3[] positions = new Vector3[ Game.Params.LineSegments ];

        Vector3 lastVelocity = _transform.position;
        positions[ 0 ] = lastVelocity;

        for ( int i = 1; i < Game.Params.LineSegments; i++ )
        {
            float time = i * Time.fixedDeltaTime;
            lastVelocity += ( _transform.forward * Game.Params.ThrowingPower + Physics.gravity * time ) * Time.fixedDeltaTime;
            positions[ i ] = lastVelocity;
        }

        return positions;
    }


    public void SetLookAt( Vector3 point )
    {
        _transform.LookAt( point );
    }

    public void Aim()
    {
        _aiming = true && _selectedGrenade.activeSelf;
    }

    public void Throw()
    {
        if ( !_aiming ) return;

        Grenade newGrenade = Instantiate( _selectedGrenade ).GetComponent<Grenade>();
        newGrenade.transform.position = _selectedGrenade.transform.position;

        newGrenade.transform.parent = null;
        newGrenade.Throw( _transform.forward * Game.Params.ThrowingPower );

        _aiming = false;
        _lineRenderer.positionCount = 0;

        _numberOfGrenades[ _grenadeIndex ]--;

        _grenadesUI[ _grenadeIndex ].SetNumber( _numberOfGrenades[ _grenadeIndex ] );

        if ( _numberOfGrenades[ _grenadeIndex ] == 0 ) _selectedGrenade.SetActive( false );
    }

    public void AddGrenade( GrenadeType type )
    {
        int index = _grenadeIndexByType[ type ];

        _numberOfGrenades[ index ]++;

        _grenadesUI[ index ].SetNumber( _numberOfGrenades[ index ] );

        if ( index == _grenadeIndex )
            _selectedGrenade.SetActive( true );
    }
}
