﻿using UnityEngine;



public class Grenade : MonoBehaviour
{
    [SerializeField] protected float _damage;
    [SerializeField] protected float _radius;


    public GrenadeType Type;

    public Color Color;


    private Rigidbody _rb;

    private bool _collision;


    public void SetActive( bool value )
    {
        gameObject.SetActive( value );
    }

    public void Throw( Vector3 force )
    {
        _rb = GetComponent<Rigidbody>();
        _rb.useGravity = true;
        _rb.isKinematic = false;
        _rb.AddForce( force, ForceMode.Impulse );

        GetComponentInChildren<BoxCollider>().isTrigger = false;

        Destroy( gameObject, 10.0f );
    }


    private void Awake()
    {
        GetComponentInChildren<MeshRenderer>().material.color = Color;
    }

    private void OnCollisionEnter( Collision collision )
    {
        if ( _collision ) return;
        _collision = true;

        GetComponent<Rigidbody>().velocity = Vector3.zero;
        ParticleSystem ps = GetComponentInChildren<ParticleSystem>();
        ps.transform.parent = null;
        ps.Play();

        Collider[] colliders = Physics.OverlapSphere( transform.position, _radius );

        OnHit( colliders );

        Destroy( gameObject );
    }


    protected virtual void OnHit( Collider[] colliders )
    {
        foreach ( Collider col in colliders )
            col.GetComponentInChildren<Enemy>()?.Hit( _damage );
    }
}
