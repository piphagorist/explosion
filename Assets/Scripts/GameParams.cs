﻿using UnityEngine;


[CreateAssetMenu]
public class GameParams : ScriptableObject
{
    [Header ("Задник активной гранаты")]
    public Color GrenadeImgActiveBG;
    [Header( "Задник неактивной гранаты" )]
    public Color GrenadeImgInactiveBG;

    [Space(10)]

    [Header( "Сила броска" )]
    public float ThrowingPower;
    [Header( "Длина траектории" )]
    public int LineSegments;

    [Space( 10 )]

    [Header( "Количество гранат на поле" )]
    public int NumberOfGrenates;
    [Header( "Количество врагов на поле" )]
    public int NubmerOfEnemies;
    [Header( "Радиус спавна" )]
    public float SpawnRadius;

    [Space( 10 )]

    [Header( "Радиус подбора гранат" )]
    public float PlayerRadius;

    [Space( 10 )]

    [Header( "Скорость игрока пешком" )]
    public float PlayerWaklSpeed;
    [Header( "Скорость игрока бегом" )]
    public float PlayerSprintSpeed;
    [Header( "Скорость вращения игрока" )]
    public float PlayerRotationSpeed;
    [Header( "Стоять при прицеливании" )]
    public bool StandToAim;
}
