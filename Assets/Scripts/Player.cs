﻿using UnityEngine;



public class Player : MonoBehaviour
{
    private Transform _transform;
    private Transform _cameraTransform;

    private Vector3 _playerRotation;
    private Vector3 _cameraRotation;

    private Launcher _launcher;

    private float _speed;

    private bool _movementIsBlocked;


    void Start()
    {
        _transform = transform;

        _cameraTransform = GetComponentInChildren<Camera>().transform;
        _launcher = GetComponentInChildren<Launcher>();

        GetComponent<SphereCollider>().radius = Game.Params.PlayerRadius;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        _speed = Input.GetKey( KeyCode.LeftShift ) || Input.GetKey( KeyCode.RightShift ) ?
            Game.Params.PlayerSprintSpeed : Game.Params.PlayerWaklSpeed;

        _playerRotation.y += Input.GetAxis( "Mouse X" ) * Game.Params.PlayerRotationSpeed;
        _cameraRotation.x -= Input.GetAxis( "Mouse Y" ) * Game.Params.PlayerRotationSpeed;

        _transform.eulerAngles = _playerRotation;
        _cameraTransform.localEulerAngles = _cameraRotation;

        _launcher.SetLookAt( _cameraTransform.position + _cameraTransform.forward * 10 );

        if ( Input.GetMouseButtonDown( 0 ) )
        {
            _launcher.Aim();
            _movementIsBlocked = true;
        }
        else if ( Input.GetMouseButtonUp( 0 ) )
        {
            _launcher.Throw();
            _movementIsBlocked = false;
        }

        if ( _movementIsBlocked && Game.Params.StandToAim ) return;


        if ( Input.GetKey( KeyCode.W ) )
            _transform.position += _transform.forward * Time.deltaTime * _speed;
        else if ( Input.GetKey( KeyCode.S ) )
            _transform.position += _transform.forward * Time.deltaTime * _speed * -1;

        if ( Input.GetKey( KeyCode.D ) )
            _transform.position += _transform.right * Time.deltaTime * _speed;
        else if ( Input.GetKey( KeyCode.A ) )
            _transform.position += _transform.right * Time.deltaTime * _speed * -1;


    }

    private void OnTriggerEnter( Collider other )
    {
        GrenadePickup grenade = other.transform.root.GetComponent<GrenadePickup>();
        if ( grenade )
        {
            _launcher.AddGrenade( grenade.Type );
            Destroy( grenade.gameObject );
        }
    }
}
