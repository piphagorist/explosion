﻿using UnityEngine;
using UnityEngine.UI;



public class GrenadeUI : MonoBehaviour
{
    [SerializeField] private Text _numberOfGrenades;

    [SerializeField] private Image _bg;
    [SerializeField] private Image _logo;



    public void SetNumber(int number)
    {
        _numberOfGrenades.text = number.ToString();
    }

    public void SetColor( Color color )
    {
        _logo.color = color;
    }

    public void SetSelet( bool value )
    {
        _bg.color = value ? Game.Params.GrenadeImgActiveBG : Game.Params.GrenadeImgInactiveBG;
    }
}
